import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoute} from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginService } from './services/login.service';
import { BaseUrlService } from './services/base-url.service';
import { FirstBlockComponent } from './first-block/first-block.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { Test1Component } from './test1/test1.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { Test2Component } from './test2/test2.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    FirstBlockComponent,
    SidenavComponent,
    Test1Component,
    HeaderComponent,
    FooterComponent,
    Test2Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoute
  ],
  providers: [
  LoginService,
  BaseUrlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
