import { Component, OnInit } from '@angular/core';
import { BaseUrlService } from '../services/base-url.service';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { LoginRequest } from '../interface/request';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginHash: any = {} ;
  private baseUrl = new BaseUrlService() ;
  constructor(
  	private loginService: LoginService,
    private router: Router
    ) { }

  ngOnInit() {
  }

  Login() {
    const loginData: LoginRequest = {
      'login': this.loginHash.userName,
      'password': this.loginHash.password,
      'subdomain': this.baseUrl.getSubDomain()
    };

    console.log('login details==>', loginData) ;

    try {

      this.loginService.getLogin(loginData).subscribe((sussess: any) => {
        console.log('success result is=>', sussess);
        if ( sussess.code === 200) {
        	alert('success');
          this.router.navigate(['dashboard']);
        }
      }, (error: any) => {
        console.log('Error result is=>', error);
      });
    }catch (e) {
      console.log(e) ;
    }

  }

}
