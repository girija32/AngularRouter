import {LoginComponent} from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FirstBlockComponent } from './first-block/first-block.component';
import { Test1Component } from './test1/test1.component';
import { Test2Component } from './test2/test2.component'

// const appRoutes: Routes = [
//   { path: 'login', component: LoginComponent },
//   { path: '', redirectTo: '/login', pathMatch: 'full'},
//   { path: '**', redirectTo: '/login' },
//   { path: 'dashboard', component: DashboardComponent},
// 	 { path: 'first-block', component: FirstBlockComponent}
// ];

@NgModule({
  imports: [
    // RouterModule.forRoot(appRoutes, {useHash: true})
    RouterModule.forRoot ([
    	{ path: 'login', component: LoginComponent },
  		{ path: '', redirectTo: '/login', pathMatch: 'full'},
  		// { path: '**', redirectTo: '/login' },
  		{ path: 'dashboard', component: DashboardComponent},
  		{ path: 'first-block', component: FirstBlockComponent},
  		{ path: 'test1', component: Test1Component },
      { path: 'test2', component: Test2Component },
    ])
  ],
  exports: [
  RouterModule
]
})
export class AppRoute { }