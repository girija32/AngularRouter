import { Injectable } from '@angular/core';
import {BaseUrlService} from '../services/base-url.service';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {LoginRequest} from '../interface/request';

@Injectable()
export class LoginService {
	private baseUrl = new BaseUrlService() ;

  constructor(private http: Http) { }

  public getLogin(loginData: LoginRequest): Observable<any> {
    const baseUrl2 = this.baseUrl.getBaseUrl() + '/login/login' ;
    return this.http.post(baseUrl2, loginData)
      .map(res => {
        return res.json() ;
      });
  }

}
