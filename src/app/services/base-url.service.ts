import { Injectable } from '@angular/core';

@Injectable()
export class BaseUrlService {

	subDomain: string ;
  	baseUrl: string ;

  constructor() {
  	this.setSubDomain('sys');
   }

   public setBaseUrl(setUrl): void {
    this.baseUrl = setUrl ;
  }

  public getBaseUrl(): string {
    return 'http://study.3mdigital.co.in';
  }

  public setSubDomain(subDomain): void {
    this.subDomain = subDomain;
  }

  public getSubDomain(): string {
    return this.subDomain;
  }
}
